import {BrowserRouter,Switch,Route, Redirect} from 'react-router-dom'
import './App.css';
import ForgetPassword from './components/Authentication/ForgetPassword';
import SignIn from './components/Authentication/SignIn';
import SignUp from './components/Authentication/SignUp';
import MainComponent from './components/Main';
console.log("hello world")



function App() {
  return (
    <BrowserRouter>
    <Switch>
      <Route exact path="/" component={MainComponent}/>
      <Route exact path="/login" component={SignIn}/>
      <Route exact path="/signup" component={SignUp}/>
      <Route exact path="/forget-password" component={ForgetPassword}/>
      {/* <ProtectedRoute exact path="/" component={Home}/>
      <ProtectedRoute exact path="/cart" component={Cart}/>
      <ProtectedRoute exact path="/restaurent/:id" component={Restaurent}/>
      <Route path="/not-found" component={NotFound}/>
      <Redirect to='not-found'/> */}
    </Switch>
    </BrowserRouter>
  );
}

export default App;
