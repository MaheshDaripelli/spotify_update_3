// importing the app.css for styling
import '../../App.css'

import {Component} from 'react'
import {AiOutlineCheck} from 'react-icons/ai'

class Song extends Component{
    render(){
        const{each} = this.props
        return(
            <div className="track">
                <div className="track__number">{each.id}</div>
                <div className="track__added">
                    <AiOutlineCheck/>
                </div>
                <div className="track__title">{each.song_name}</div>
                <div className="track__length">{each.song_time}</div>
                <div className="track__popularity">
                    {/* <i className="ion-arrow-graph-up-right" />
                    <FontAwesomeIcon icon="fa-solid fa-arrow-trend-down" /> */}
                    <i class="fa-solid fa-arrow-trend-down"></i>
                </div>
            </div>
        )
    }
}
export default Song