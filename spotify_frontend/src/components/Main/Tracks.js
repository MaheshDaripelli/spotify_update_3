import { Component } from "react";
import {AiOutlineCheck} from 'react-icons/ai'
class Tracks extends Component {
  render() {
    const { tracks } = this.props;
    return (
      <>
        <div className="track">
          <div className="track__art">
            <img src={tracks.image} alt="When It's Dark Out" />
          </div>
          <div className="track__number">{tracks.number}</div>
          <div className="track__added">
          <AiOutlineCheck/>
          </div>
          <div className="track__title">{tracks.song_name}</div>
          <div className="track__explicit"></div>
          <div className="track__plays">{tracks.listner_count}</div>
        </div>
      </>
    );
  }
}

export default Tracks;
