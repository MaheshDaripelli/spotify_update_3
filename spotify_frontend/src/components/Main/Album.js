import { Component } from "react";

class Album extends Component{
    render(){
        return(
            <div className="album__info">
                <div className="album__info__art">
                    <img
                      src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/7022/whenDarkOut.jpg"
                      alt="When It's Dark Out"
                    />
                </div>
                <div className="album__info__meta">
                    <div className="album__year">2015</div>
                    <div className="album__name">When It's Dark Out</div>
                    <div className="album__actions">
                      <button className="button-light save">Save</button>
                      <button className="button-light more">
                      <i className="ion-ios-more" />
                      </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Album