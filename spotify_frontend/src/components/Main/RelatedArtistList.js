import { Component } from "react";
import RelatedArtist from "./RelatedArtist";

class RelatedArtistList extends Component{
    render(){
        const{related_artist} = this.props.related_artist
        return(
            <div role="tabpanel" className="tab-pane" id="related-artists">
              <div className="media-cards">
                {
                    related_artist.map(each=>(
                        <RelatedArtist each={each}/>
                    ))
                }
              </div>
            </div>
        )
    }
}

export default RelatedArtistList