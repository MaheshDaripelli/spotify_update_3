import { Component } from "react";
import {BsThreeDots}from 'react-icons/bs';
class ArtistInfo extends Component{
    render(){
        const{main_panel} = this.props.artist_data
        return(
            <>
                <div className="artist__info">
                <div className="profile__img">
                <img
                    src={main_panel.artist.artist_image}
                    alt="G-Eazy"
                />
                </div>
                <div className="artist__info__meta">
                <div className="artist__info__type">Artist</div>
                <div className="artist__info__name">{main_panel.artist.name}</div>
                <div className="artist__info__actions">
                    <button className="button-dark">
                    <i className="ion-ios-play" />
                    Play
                    </button>
                    <button className="button-light">Follow</button>
                    <button className="button-light more">
                    {/* <i className="ion-ios-more" /> */}
                    <BsThreeDots style={{marginLeft:"7px"}}/>
                    </button>
                </div>
                </div>
            </div>
            <div className="artist__listeners">
                <div className="artist__listeners__count">{main_panel.artist.monthly_listeners}</div>
                <div className="artist__listeners__label">Monthly Listeners</div>
            </div>
          </>
        )
    }
}

export default ArtistInfo