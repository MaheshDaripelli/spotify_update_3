import { Component } from "react";
import './body.css'

class Body extends Component{
    render(){
        const hours = new Date().getHours()
        const greeting = (hours<12) ? "Morning" : (hours<17) ? "Afternoon" : "Evening"

        return(
            <main>
      <div className="greeting">
        <h2>Good {greeting}</h2>
      </div>
      
      <div className="recent-activity">
        <div className="activity-info">
          <div className="img-div">
            <img src="https://seed-mix-image.spotifycdn.com/v6/img/pop/4AK6F7OLvEQ5QYCBNiQWHq/en/default" alt="Pop Mix playlist cover photo" />
          </div>
          <p>Pop Mix</p>
        </div>
        <div className="activity-info">
          <div className="img-div">
            <img src="https://i.scdn.co/image/ab67656300005f1f854bce22cb0e6890dba92dd8" alt="The Athletic Football Tactics Podcast cover photo" />
          </div>
          <p>The Athletic Football Tactics Podcast</p>
        </div>
        <div className="activity-info">
          <div className="img-div">
            <img src="https://i.scdn.co/image/ab67706f00000002b75cdf3f088c129cc350c0f8" alt="This Is One Direction playlist cover photo" />
          </div>
          <p>This Is One Direction</p>
        </div>
        <div className="activity-info">
          <div className="img-div">
            <img src="https://i.scdn.co/image/ab6761610000e5eb6e9a17ce6d67c02312e3fb89" alt="Alessia Cara cover photo" />
          </div>
          <p>Alessia Cara</p>
        </div>
        <div className="activity-info">
          <div className="img-div">
            <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb26dbdbdacda5c30dc95e0c2c/3/en/default" alt="Daily Mix 3 playlist cover photo" />
          </div>
          <p>Daily Mix 3</p>
        </div>
        <div className="activity-info">
          <div className="img-div">
            <img src="https://i.scdn.co/image/ab67706f000000021373358fa4ff03aac54f188e" alt="All Out 10s playlist cover photo" />
          </div>
          <p>All Out 10s</p>
        </div>
      </div>
      
      <div className="category">
        <div className="title">
          <h3>Your Shows</h3>
          <a href="#">SEE ALL</a>
        </div>
        <div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/a3313c9ff4f806345e71728b502022782e92cf34" alt="HTML All The Things podcast cover photo" />
            </div>
            <p className="category-name">HTML All The Things</p>
            <p className="author">Show. Matt & Mike</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1f854bce22cb0e6890dba92dd8" alt="The Athletic Football Podcast cover photo" />
            </div>
            <p className="category-name">The Athletic Football Podcast</p>
            <p className="author">Show. The Athletic</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1fcf5b0c37fe67ebbcdceb930b" alt="Headline: Breaking Football News playlist cover photo" />
            </div>
            <p className="category-name">Headline: Breaking Football News</p>
            <p className="author">Show. The Athletic</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/fedc8e1c8b93cc9b8e49e8e101ec9d9b8795d1fe" alt="Raj Prakash Paul playlist cover photo" />
            </div>
            <p className="category-name">Raj Prakash Paul</p>
            <p className="author">Show. Raj Prakash Paul</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1f6d655e4364ad1ed1dad7a83d" alt="The Here We Go Podcast cover photo" />
            </div>
            <p className="category-name">The Here We Go Podcast</p>
            <p className="author">Show. Here We Go</p>
          </div>
        </div>
      </div>
      
      <div className="category">
        <div className="title">
          <h3>Made For Rajesh Alluri</h3>
          <a href="#">SEE ALL</a>
        </div>
        <div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb031619e5eb9ed3b9806b648b/1/en/default" alt="Daily Mix 1 cover photo" />
            </div>
            <p className="category-name">Daily Mix 1</p>
            <p className="author">Ella Mai, Shawn Mendes, Jason Derulo</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb6e9a17ce6d67c02312e3fb89/2/en/default" alt="Daily Mix 2 cover photo" />
            </div>
            <p className="category-name">Daily Mix 2</p>
            <p className="author">Alessia Cara, 5 Seconds Of Summer</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb26dbdbdacda5c30dc95e0c2c/3/en/default" alt="Daily Mix 3 cover photo" />
            </div>
            <p className="category-name">Daily Mix 3</p>
            <p className="author">Taylor Swift, Fifth Harmony</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb7aff8a274fcec288dd534abc/4/en/default" alt="Daily Mix 4 cover photo" />
            </div>
            <p className="category-name">Daily Mix 4</p>
            <p className="author">Jesus Culture, Bethel Music</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://dailymix-images.scdn.co/v2/img/ab6761610000e5eb70859a2e628fd00e8be3a696/5/en/default" alt="Daily Mix 5 cover photo" />
            </div>
            <p className="category-name">Daily Mix 5</p>
            <p className="author">Benny Joshua, Allen Ganta</p>
          </div>
        </div>
      </div>
      
      <div className="category">
        <div className="title">
          <h3>Charts</h3>
          <a href="#">SEE ALL</a>
        </div>
        <div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://charts-images.scdn.co/assets/locale_en/regional/daily/region_in_default.jpg" alt="Top 50 India playlist cover photo" />
            </div>
            <p className="category-name">Top 50 India</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706f00000002b545db24c5864981ff896f07" alt="Hot Hits India playlist cover photo" />
            </div>
            <p className="category-name">Hot Hits India</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://charts-images.scdn.co/assets/locale_en/regional/daily/region_global_default.jpg" alt="Top 50 Global playlist cover photo" />
            </div>
            <p className="category-name">Top 50 Global</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706c0000da84fc156bed23ef2df5814fb190" alt="Top Albums - Global playlist cover photo" />
            </div>
            <p className="category-name">Top Albums - Global</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://charts-images.scdn.co/assets/locale_en/viral/daily/region_global_default.jpg" alt="Viral 50 - India playlist cover photo" />
            </div>
            <p className="category-name">Viral 50 - India</p>
          </div>
        </div>
      </div>
      
      <div className="category">
        <div className="title">
          <h3>Best Of Artists</h3>
          <a href="#">SEE ALL</a>
        </div>
        <div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706f000000021230c7f75023a90181e914a0" alt="This is Alessia Cara playlist cover photo" />
            </div>
            <p className="category-name">This Is Alessia Cara</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706f00000002a0a577ed169a7792c9363d6c" alt="This is Hillsong Worship playlist cover photo" />
            </div>
            <p className="category-name">This Is Hillsong Worship</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706f00000002181896dd694bc09e4a0f13c8" alt="This is Camila Cabello playlist cover photo" />
            </div>
            <p className="category-name">This Is Camila Cabello</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706f000000027988283d13d5654287988494" alt="This is Shawn Mendes playlist cover photo" />
            </div>
            <p className="category-name">This Is Shawn Mendes</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67706f00000002b75cdf3f088c129cc350c0f8" alt="This is One Direction playlist cover photo" />
            </div>
            <p className="category-name">This Is One Direction</p>
          </div>
        </div>
      </div>
      
      <div className="category final-category">
        <div className="title">
          <div className="popular-shows">
            <p>POPULAR WITH LISTENERS OF</p>
            <h3>Headline: Breaking Football News</h3>
          </div>
          <a href="#">SEE ALL</a>
        </div>
        <div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1f6070c8c3beddfeef90cd9044" alt="Football Cliches podcast cover photo" />
            </div>
            <p className="category-name">Football Cliches</p>
            <p className="author">Show. The Athletic</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1fff3db692e1f2dbe7c73951e2" alt="The Athletic Football Podcast podcast cover photo" />
            </div>
            <p className="category-name">The Athletic Football Podcast</p>
            <p className="author">Show. The Athletic</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1fea8a7821ffed11a7bfe73c71" alt="Beyond the Headline podcast cover photo" />
            </div>
            <p className="category-name">Beyond the Headline</p>
            <p className="author">Show. The Athletic</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/ab67656300005f1f31a9464d4951d231128babc6" alt="The Next Big Thing podcast cover photo" />
            </div>
            <p className="category-name">The Next Big Thing</p>
            <p className="author">Show. The Athletic</p>
          </div>
          <div className="category-info">
            <div className="img-div">
              <img src="https://i.scdn.co/image/bdd990bddb85baa44c320b2ffba328549e184643" alt="The Scouted Football Podcast podcast cover photo" />
            </div>
            <p className="category-name">The Scouted Football Podcast</p>
            <p className="author">Show. Scouted Football</p>
          </div>
        </div>
      </div>
    </main>
        )
    }
}
export default Body