import {Component} from 'react'
import {AiOutlineDown} from 'react-icons/ai';
import {FiHeadphones} from 'react-icons/fi';
import {IoMusicalNotesSharp} from  'react-icons/io5';
import {IoIosPerson} from 'react-icons/io';
import {IoDocumentOutline} from "react-icons/io5";
class Music extends Component{
  render(){
    return(
      <>
      <div className="navigation__list">
          <div
            //className="navigation__list__header"
            role="button"
            data-toggle="collapse"
            href="#yourMusic"
            aria-expanded="true"
            aria-controls="yourMusic"
          >
            Your Music
            <AiOutlineDown/>
          </div>
          <div className="collapse in" id="yourMusic">
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-headphone" /> */}
              <FiHeadphones/>
              <span>Songs</span>
            </a>
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-ios-musical-notes" /> */}
              <IoMusicalNotesSharp/>
              <span>Albums</span>
            </a>
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-person" /> */}
              <IoIosPerson/>
              <span>Artists</span>
            </a>
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-document" /> */}
              <IoDocumentOutline/>
              <span>Local Files</span>
            </a>
          </div>
        </div>
      </>
    )
  }
}

export default Music