import {Component} from "react"
import {IoMusicalNotesSharp} from "react-icons/io5";
import {AiOutlineDown} from 'react-icons/ai';
class Playlist extends Component{
  render(){
    return(
      <div className="navigation__list">
          <div
            //className="navigation__list__header"
            role="button"
            data-toggle="collapse"
            href="#playlists"
            aria-expanded="true"
            aria-controls="playlists"
          >
            Playlists
            <AiOutlineDown/>
            
          </div>
          <div className="collapse in" id="playlists">
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Doo Wop</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Pop Classics</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Love $ongs</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Hipster</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>New Music Friday</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Techno Poppers</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Summer Soothers</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Hard Rap</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Pop Rap</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>5 Stars</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Dope Dancin</span>
            </a>
            <a href="#" className="navigation__list__item">
            <IoMusicalNotesSharp/>
              <span>Sleep</span>
            </a>
          </div>
        </div>
    )
  }
}

export default Playlist