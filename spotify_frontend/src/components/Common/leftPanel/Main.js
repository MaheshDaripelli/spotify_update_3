import {Component} from 'react'
import {AiOutlineDown} from 'react-icons/ai';
import {IoIosBrowsers} from "react-icons/io";
import {MdPeople} from 'react-icons/md';
import {IoRadio} from 'react-icons/io5'
 

class Main extends Component{
  render(){
    return(
      <div className="navigation__list">
          <div
            //className="navigation__list__header"
             role="button"
           data-toggle="collapse"
            href="#main"
            aria-expanded="true"
            aria-controls="main"
            
          >
            Main
            <AiOutlineDown/>
          </div>
          <div className="collapse in" id="main">
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-ios-browsers" /> */}
              <IoIosBrowsers/>
              <span>Browse</span>
            </a>
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-person-stalker" /> */}
              <MdPeople/>
              <span>Activity</span>
            </a>
            <a href="#" className="navigation__list__item">
              {/* <i className="ion-radio-waves" /> */}
              <IoRadio/>
              <span>Radio</span>
            </a>
          </div>
        </div>
    )
  }
}

export default Main