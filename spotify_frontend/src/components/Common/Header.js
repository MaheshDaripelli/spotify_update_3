import {Component} from 'react'

class Header extends Component{
  render(){
    const {header} = this.props.header
    return(
      <section className="header">
  
      <div className="page-flows">
        <span className="flow">
        <img className='img1' src="https://www.pngkey.com/png/full/190-1907978_spotify-logo-png-white-spotify-logo-white-transparent.png" id="logo"/>
        </span>
      </div>
      <div className="search">
        <input type="text" placeholder="Search" className='input1' />
      </div>
      <div className="user">
        <div className="user__notifications">
        <i class='far fa-bell'></i>
        </div>
        <div className="user__inbox">
        <i  class='fas fa-download'/>
        </div>
        <div className="user__info">
          <span className="user__info__img">
            <img
              src={header.profile_image}
              alt="Profile Picture"
              className="img-responsive"
            />
          </span>
          <span className="user__info__name">
            <span className="first">{header.name}</span>
            
          </span>
        </div>
        <div className="user__actions">
          <div className="dropdown">
            <button
              className="dropdown-toggle"
              type="button"
              id="dropdownMenu1"
              data-toggle="dropdown"
              aria-haspopup="true"
              aria-expanded="true"
            >
              <i className="ion-chevron-down" />
            </button>
            <ul
              className="dropdown-menu dropdown-menu-right"
              aria-labelledby="dropdownMenu1"
            >
              <li>
                <a href="#">Private Session</a>
              </li>
              <li>
                <a href="#">Account</a>
              </li>
              <li>
                <a href="#">Settings</a>
              </li>
              <li>
                <a href="#">Log Out</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
    )
  }
}

export default Header