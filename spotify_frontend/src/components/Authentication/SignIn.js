import { Component } from "react";

class SignIn extends Component{
  state = {
    username:"",
    password:"",
    error_msg:"",
    is_login:false
  }
  redirectToSignup = ()=>{
    const{history} = this.props
    history.replace("/signup")
  }

  loginSuccess = (data)=>{
    const{history} = this.props
    history.replace("/")
  }

  submitingLoginForm = async(event) => {
    event.preventDefault()
    const url = "http://localhost:3001/login/user"
    const{username,password} = this.state
    const userDetails = {
      username,
      password
    }
    const option = {
      method:"POST",
      body: JSON.stringify(userDetails),
      headers : { 
        'Content-Type': 'application/json',
        'Accept': 'application/json'
       }
    }
    const response = await fetch(url,option)
    const data = await response.json()
    if(data.status_code !== 200){
      this.setState({error_msg:data.status_message,is_login:true})
    }else{
      this.loginSuccess(data)
    }
    
  }

  changeUsername=(event)=>{
    this.setState({username:event.target.value})
  }

  changePassword=(event)=>{
    this.setState({password:event.target.value})
  }

    render(){
      const{username,password,is_login,error_msg} = this.state
        return(
            <div>
        <header className="top-header">
        </header>
        <div id="mainCoantiner">
          <div className="main-header">
          <img className='img2' src="https://www.pngkey.com/png/full/190-1907978_spotify-logo-png-white-spotify-logo-white-transparent.png" id="logo"/>
            <div className="folio-btn">
              <a className="folio-btn-item ajax" href="https://bit.ly/2y6huFG" target="_blank"><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /><span className="folio-btn-dot" /></a>
            </div>
          </div>
          {/*dust particel*/}
          <div>
            <div className="starsec" />
            <div className="starthird" />
            <div className="starfourth" />
            <div className="starfifth" />
          </div>
          {/*Dust particle end-*/}
          <div className="container text-center text-dark mt-5">
            <div className="row">
              <div className="col-lg-4 d-block mx-auto mt-5">
                <div className="row">
                  <div className="col-xl-12 col-md-12 col-md-12">
                    <div className="card">
                      <div className="card-body wow-bg" id="formBg">
                        <h3 className="colorboard">Login</h3>
                        <p className="text-muted">Sign In to your account</p>
                        <form onSubmit={this.submitingLoginForm}>
                          <div className="input-group mb-3"> <input type="text" className="form-control textbox-dg" placeholder="Username" value={username} onChange={this.changeUsername} /> </div>
                          <div className="input-group mb-4"> <input type="password" className="form-control textbox-dg" placeholder="Password" value={password} onChange={this.changePassword} /> </div>
                          <div className="col-12"> <button type="submit" className="btn btn-primary btn-block logn-btn button2">Login</button> </div>
                          {is_login && <p className="register-error">*{error_msg}</p>}
                        </form>
                        <div className="row">
                          <div className="col-12"> <a href="forgot-password.html" className="btn btn-link box-shadow-0 px-0">Forgot password?</a> </div>
                        </div>
                        <p className="additional-act">Don't have account? <span onClick={this.redirectToSignup}> Sign up </span></p>
                        <div className="mt-6 btn-list">
                          <button type="button" className="socila-btn btn btn-icon btn-facebook fb-color"><a href="https://www.facebook.com/" target="_blank"><i className="fab fa-facebook-f faa-ring animated" /></a></button> <button type="button" className="socila-btn btn btn-icon btn-google incolor"><a href="https://www.linkedin.com/" target="_blank"><i className="fab fa-linkedin-in faa-flash animated" /></a></button> <button type="button" className="socila-btn btn btn-icon btn-twitter tweetcolor"><a href="https://www.twitter.com/" target="_blank"><i className="fab fa-twitter faa-shake animated" /></a></button> <button type="button" className="socila-btn btn btn-icon btn-dribbble driblecolor"><a href="https://www.google.com/" target="_blank"><i className="fab fa-dribbble faa-pulse animated" /></a></button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
        )
    }
}
export default SignIn