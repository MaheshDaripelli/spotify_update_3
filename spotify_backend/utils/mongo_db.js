// importing mangoose from mangoose
import mongoose from "mongoose";
// import register_data_schema from schema.js file
import {
  USER_REGISTER_SCHEMA,
  USER_LOGIN_SCHEMA,
  ADMIN_LOGIN_SCHEMA,
  ADMIN_REGISTER_SCHEMA,
  ArtistDetails,
  UserRatingSchema,
} from "./schema.js";
// connecting the db
mongoose.connect(
  "mongodb+srv://Mahesh:WmysRZlFWrNCoeUY@cluster0.9brlo.mongodb.net/zomato?retryWrites=true&w=majority"
);
//mongoose.connect("mongodb+srv://nmanchiravula:Nishitha1997@cluster0.j8eac.mongodb.net/zomato?retryWrites=true&w=majority")

/* creating a model for the user_lregister_data for creating a list and enter objects in it */
const USER_REGISTER_DATA = mongoose.model(
  "USER_REGISTER_DATA",
  USER_REGISTER_SCHEMA
);

/* creating a model for the user_login_data for storing the user login details */
const USER_LOGIN_DATA = mongoose.model("USER_LOGIN_DATA", USER_LOGIN_SCHEMA);

/* creating a model for the user_login_data for storing the user login details */
const ADMIN_LOGIN_DATA = mongoose.model("ADMIN_LOGIN_DATA", ADMIN_LOGIN_SCHEMA);

/* creating a model for the admin_lregister_data for creating a list and enter objects in it */
const ADMIN_REGISTER_DATA = mongoose.model(
  "ADMIN_REGISTER_DATA",
  ADMIN_REGISTER_SCHEMA
);


/* creating a model for the SPOTIFY_ARTIST_DETAILS for creating a list and enter objects in it */
const SPOTIFY_ARTIST_DETAILS = mongoose.model(
  "SPOTIFY_ARTIST_DETAILS",
  ArtistDetails
);


function inserting_register_data_into_database(register_data) {
  // const USER_LOGIN_data_1 = mongoose.model('USER_LOGIN_data_1', USER_LOGINPAGE_SCHEMA ); // schema
  const user_data = new USER_REGISTER_DATA({
    name: register_data.name,
    username: register_data.username,
    password: register_data.password,
    //re_enter_password: register_data.re_enter_password,
    email: register_data.email,
    phone_number: register_data.phone_no,
    security_question: register_data.security_question,
  });
  // saving the data
  user_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

function inserting_admin_register_data_into_database(admin_register_data) {
  // const USER_LOGIN_data_1 = mongoose.model('USER_LOGIN_data_1', USER_LOGINPAGE_SCHEMA ); // schema
  const admin_data = new ADMIN_REGISTER_DATA({
    name: admin_register_data.name,
    username: admin_register_data.username,
    password: admin_register_data.password,
    //re_enter_password: admin_register_data.re_enter_password,
    email: admin_register_data.email,
    phone_number: admin_register_data.phone_no,
    security_question: admin_register_data.security_question,
  });
  // saving the data
  admin_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

function inserting_user_login_data_into_database(login_data) {
  var date = new Date()
  login_data.date = date
  const user_login_data = new USER_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  // saving the data
  user_login_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

function inserting_admin_login_data_into_database(login_data) {
  var date = new Date()
  login_data.date = date
  const user_login_data = new ADMIN_LOGIN_DATA({
    username: login_data.username,
    password: login_data.password,
    jwt_token: login_data.jwt_token,
    logon_time: login_data.date,
  });
  // saving the data
  user_login_data.save().then(() => console.log("data saved")).catch((err) => {console.log(err);});
}

/**
 * PostRestaurants function takes params
 * create new restaurant
 * push the data into the database of the RestaurantScheme
 * @param {request_params} to pass the arguments from the requested body
 */
function PostRestaurants(request_params) {
  const restaurant = new Restaurant({
    name: request_params.name,
    address: request_params.address,
    imageUrl: request_params.imageUrl,
    cuisine: request_params.cuisine,
    user_rating: request_params.user_rating,
  });
  restaurant.save().then(() => console.log("data saved"));
}

/**
 * GetRestaurnts function fetch all the restaurants or retuns error if any
 */
function GetRestaurants(response) {
  Restaurant.find({},(err,data)=>{
    if(err){
      console.log(err)
    }else{
      response.send(data)
    }
  })
}

/**
 * FetchRestaurant function is used to get restaurant using id
 * @param {id} is to get restaurant based on id
 */
function FetchRestaurant(id) {
  Restaurant.findById(id)
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

/**
 * PostFoodItems function takes the params
 * push the data into the database
 * @param {request_params} to pass the arguments from the requested body
 */
function PostFoodItems(request_params) {
  const FoodItem = new FoodItems({
    restaurantId: request_params.restaurantId,
    name: request_params.name,
    imageUrl: request_params.imageUrl,
    foodtype: request_params.foodtype,
    rating: request_params.rating,
  });
  FoodItem.save().then(() => console.log("data saved"));
}

/**
 * Function to fetch food items of the restaurant using id
 * @param {string} id is the restaurant id
 */
function FetchFoodItemOfRestaurant(id,response) {
  FoodItems.find({ restaurantId: id })
    .then((data) => {
      response.send(data)
    })
    .catch((err) => {
      console.log(err);
    });
}

/**
 * Userrating function takes params
 * push the data into the database
 * @param {request_params} to pass the arguments from the requested body
 */
function UserRating(request_params) {
  const Ratings = new UserRatings({
    rating: request_params.rating,
    rating_text: request_params.rating_text,
    rater: request_params.rater,
    ratee: request_params.ratee,
  });
  Ratings.save().then(() => console.log("data saved"));
}

/**
 * Function to fetch user ratings of user using user id and restaurant id
 * @param {string} user_id,res_id is the restaurant id and user id
 */
function Ratings(user_id, res_id) {
  UserRatings.find({ rater: user_id, ratee: res_id })
    .then((data) => {
      console.log(data);
    })
    .catch((err) => {
      console.log(err);
    });
}

/* exporting the inserting_register_data_into_database, 
        inserting_user_login_data_into_database, inserting_admin_register_data_into_database
         functions and USER_REGISTER_DATA, ADMIN_REGISTER_DATA instaces.
    */
export {
  inserting_register_data_into_database,
  USER_REGISTER_DATA,
  ADMIN_REGISTER_DATA,
  USER_LOGIN_DATA,
  ADMIN_LOGIN_DATA,
  SPOTIFY_ARTIST_DETAILS,
  inserting_user_login_data_into_database,
  inserting_admin_register_data_into_database,
  inserting_admin_login_data_into_database,
  PostRestaurants,
  FetchRestaurant,
  PostFoodItems,
  FetchFoodItemOfRestaurant,
  GetRestaurants,
  UserRating,
  Ratings,
};
