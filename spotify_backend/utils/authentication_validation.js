// importing the USER_REGISTER_DATA, ADMIN_REGISTER_DATA instances from the mongodb.js 
import
    {
    USER_REGISTER_DATA,
    ADMIN_REGISTER_DATA,
    USER_LOGIN_DATA,
    ADMIN_LOGIN_DATA
    } 
    from '../utils/mongo_db.js' 

/* importing inserting_register_data_into_database,
        inserting_user_login_data_into_database,
        inserting_admin_register_data_into_database
        from the mongodb.js for storing the register data */
import 
    {
    inserting_register_data_into_database,
    inserting_admin_register_data_into_database,
    inserting_user_login_data_into_database,
    inserting_admin_login_data_into_database
    } 
    from '../utils/mongo_db.js'

/*  conflict_message
    from the status_codes.js for sending the response messages */
import
    {
    conflict_message,
    user_alredy_login
    }
    from './status_codes.js'

/* importing the jwt_token_generator is generating the jwt_token for every function*/
import { jwt_token_generator } from "./jwt_generator.js";

/**
 * Validates all the body_params that are used in regester.
 * @param {1} body_params for which all the params has to be verified.
 * @returns true - if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails. 
 */
 function validate_register_data(body_params) {
    // checking every param if anyone of them is not provided-'406' :not acceptable
    if (Object.keys(body_params).length !== 7){
        //if required number of params are not there it will send 421 status code
        return {status_code:406, status_message:"Parameters count is not matches with database fields"};
    }
    let body_keys_list = []
    for(let item in body_params){
        body_keys_list.push(item)
    }
    //delaring a list of keys to validate the keys
    let key_list = ["name","username","re_enter_password","password","email","phone_no","security_question"];
    for (let key of key_list) {
        if (body_keys_list.includes(key)) {
            continue;
        }
        else {
            let msg = key + " is Missing!";
            return {status_code:406, status_message:msg};
        }
    }
    let {name,username,password,re_enter_password,email,phone_no,security_question} = body_params;
    //validating the password using _validate_name()
    let name_response = _validate_name(name);
    if (name_response.status_code !== 200) {
        return name_response;
    }
    //validating the password using _validate_password()
    let password_response = _validate_password(password);
    if (password_response.status_code !== 200) {
        return password_response;
    }
    //validating the password using _validate_password()
    let re_enter_password_response = re_enter_password_validator(password,re_enter_password);
    if (re_enter_password_response.status_code !== 200) {
        return re_enter_password_response;
    }
    //validating the username using _validate_name()
    let username_response = _validate_name(username);
    if (username_response.status_code !== 200) {
        return username_response;
    }
    //validating the email from validate_email
    let validate_email_response = validate_email(email);
    if (validate_email_response.status_code !== 200) {
        return validate_email_response;
    }
    //validating the email from validate_phone_number
    let validate_phone_number_response = validate_phone_number(phone_no);
    if (validate_phone_number_response.status_code !== 200) {
        return validate_phone_number_response;
    }
    return {status_code:200};
}

/**
 * To validate the name given by the user for registering
 * @param {1} name used to validate various conditions 
 * @returns true - if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.            
 */
function _validate_name(name) {
    if (typeof(name)!=="string") { 
        return {status_code:406, status_message:"Name/useranme should be a string!"};
    }
    if ((name.length < 3) || (name.length > 20)) {
        return {status_code:411, status_message:"Length of name/username should be in between 3 and 15"};
    }
    for (let index=0; index<name.length-1; index++) {
        if (name[index] === " " && name[index+1] === " ") {
            return {status_code:406, status_message:"Name/username should not contain consecutive spaces!"};
        }
    }
    return {status_code:200};
}

/**
 * validate the user register data based on the user register database data
 * @param register_data that contains the user register data  
 * @param response which is the response object that send the response to the client
 * @param validation_result which is the object that contains the user register validation result
 * returned from the validate_register_data
 * @returns the different response like if user name exists in the user_register_data db it returns the 
 * conflict 409 if username not exists it stores the user_register data into the user register database
 */
function validate_user_register_database(register_data,response,validation_result){
    USER_REGISTER_DATA.find(
        { username: `${register_data.username}` },
        (error, data) => {
          // if there is any error in the data fetching it prints the error message
          if (error) {
            console.log(error);
          } else {
            // checking the username consists the database or not
            if (data.length !== 0) {
              // sending the conflict message
              response.send(JSON.stringify(conflict_message));
            } else {
              // Ta-Daaa this is the happy scenario, send 200 OK statusmessage created succesfully
              inserting_register_data_into_database(register_data);
              // 200 ok user created successfully
              response.send(JSON.stringify(validation_result));
            }
          }
        }
      );
}

/**
 * validate the user register data based on the user register database data
 * @param register_data that contains the admin register data  
 * @param response which is the response object that send the response to the client
 * @param validation_result which is the object that contains the user register validation result
 * returned from the validate_register_data
 * @returns the different response like if user name exists in the admin_register_data db it returns the 
 * conflict 409 if username not exists it stores the admin_register data into the user register database
 */
 function validate_admin_register_database(register_data,response,validation_result){
   ADMIN_REGISTER_DATA.find({username:`${register_data.username}`},(err,data)=>{
     if(err){
       console.log(err)
     }else{
       if(data.length !== 0){
        response.send("username already exists in user database")
       }else{
        ADMIN_REGISTER_DATA.find(
          { username: `${register_data.username}` },
          (error, data) => {
          // if there is any error in the data fetching it prints the error message
            if (error) {
              console.log(error);
            } else {
              // checking the username consists the database or not
              if (data.length !== 0) {
                // sending the conflict message
                response.sendStatus(conflict_message.status_code);
              } else {
                // Ta-Daaa this is the happy scenario, send 200 OK statusmessage created succesfully
                inserting_admin_register_data_into_database(register_data);
                // 200 ok user created successfully
                response.send(validation_result);
              }
            }
          }
        );
       }
     }
   })
}

// async function is_login_before(login_data){
//   await USER_LOGIN_DATA.find({"username":`${login_data.username}`},(err,data)=>{
//     if(err){
//       console.log(err)
//     }else{
//       if(data.length === 0){
//         return true
//       }
//     }
//   })
// }

const deleting_user_login_details_in_data_base = (id)=>{
  USER_LOGIN_DATA.findOneAndRemove({id:id},(err)=>{
      if(err){
          console.log(err)
      }
  })
};

const deleting_admin_login_details_in_data_base = (id)=>{
  ADMIN_LOGIN_DATA.findOneAndRemove({id:id},(err)=>{
      if(err){
          console.log(err)
      }
  })
};


/**
 * 
 */
const update_user_password = async(username,new_password)=>{
  try{
    const result = await USER_REGISTER_DATA.updateOne({username:username},{
      $set:{
        password:new_password
      }
    })
    console.log(result)
  }catch(err){
    console.log(err)
  }
}

const update_admin_password = async(useranme,new_password)=>{
  try{
    const result = await ADMIN_REGISTER_DATA.updateOne({useranme},{
      $set:{
        password:new_password
      }
    })
    console.log(result)
  }catch(err){
    console.log(err)
  }
}


/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data  
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
function validate_user_login_data(login_data,response){
   // fetching the user login data
    USER_LOGIN_DATA.find({"username":`${login_data.username}`},(err,data)=>{
        if(err){
          console.log(err)
        }else{
          if(data.length === 0){
            // fetching the user register data from the data base for login validatinn
    USER_REGISTER_DATA.find({ username: login_data.username }, (err, data) => {
      // any error occur in the data fetching it prints in the console
      if (err) {
        // printing error in console
        console.log(err);
        //if there is no error in the data fetching it goes through the else block
      } else {
        // if there any data that matches with the username it goes through the if condition
        if (data.length !== 0) {
          // iterating over the data fetched from the database
          for (let item of data) {
            // validating the password
            if (item.password === login_data.password) {
              // if password is valid it sends the response as 200 'success'
              // const x = is_login_before(login_data)
              // console.log(x)
              login_data.status_code = 200
              const jwt_token = jwt_token_generator(login_data.username);
              login_data.jwt_token = jwt_token;
              inserting_user_login_data_into_database(login_data);
              response.send(JSON.stringify(login_data));
              break;
            }
            // if password is invalid it sends the response as password not match
            else {
              const obj = {
                status_code:401,
                status_message:"invalid password"
              }
              response.send(JSON.stringify(obj));
              break;
            }
          }
        }
        // if fetching data count is empty it sends the response as user not found
        else {
          const obj = {
            status_code:404,
            status_message:"username not found"
          }
          response.send(JSON.stringify(obj));
        }
      }
    });
          }else{
            var message = user_alredy_login(login_data.username)
            response.send(JSON.stringify(message))
          }
        }
      })
}

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data  
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
 function validate_user_login_data_for_forget_password(login_data,response,client_key){
  const result = _validate_password(login_data.new_password)
  if(result.status_code !== 200){
    response.send(result.status_message)
  }else{
   // fetching the user login data
  USER_LOGIN_DATA.find({"jwt_token":`${client_key}`},(err,data)=>{
   if(err){
     console.log(err)
   }else{
     if(data.length !== 0){
       deleting_user_login_details_in_data_base(data[0].id)
       //fetching the user register data from the data base for login validatinn
      USER_REGISTER_DATA.find({ username: login_data.username,security_question:login_data.security_question }, (err, data) => {
        // any error occur in the data fetching it prints in the console
        if (err) {
          // printing error in console
          console.log(err);
          //if there is no error in the data fetching it goes through the else block
        } else {
          // if there any data that matches with the username it goes through the if condition
          if (data.length !== 0) {
            // iterating over the data fetched from the database
            update_user_password(data[0].username,login_data.new_password)
            response.send("password updated successfully")
          }
          // if fetching data count is empty it sends the response as user not found
          else {
            response.send("Please enter correct username or security question");
          }
        }
      });
     }else{
      //  var message = user_alredy_login(login_data.username)
      //  response.send(JSON.stringify(message))
      response.send("your not a loged in user")
     }
   }
 })
  }
}

/**
 * validate the admin login data based on the admin register database data
 * @param login_data that contains the admin login data  
 * @param response which is the response object that send the response to the client
 * @param client_key it contains the jwt_token that are passed from the request headers
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the admin register database data
 */
 function validate_admin_login_data_for_forget_password(login_data,response,client_key){
  const result = _validate_password(login_data.new_password)
  if(result.status_code !== 200){
    response.send(result.status_message)
  }else{
   // fetching the user login data
  ADMIN_LOGIN_DATA.find({"jwt_token":`${client_key}`},(err,data)=>{
   if(err){
     console.log(err)
   }else{
     if(data.length !== 0){
      deleting_admin_login_details_in_data_base(data[0].id)
       //fetching the user register data from the data base for login validatinn
      ADMIN_REGISTER_DATA.find({ username: login_data.username}, (err, data) => {
        // any error occur in the data fetching it prints in the console
        if (err) {
          // printing error in console
          console.log(err);
          //if there is no error in the data fetching it goes through the else block
        } else {
          // if there any data that matches with the username it goes through the if condition
          if (data.length !== 0) {
            console.log(data)
            // updating the password by calling the update function
            update_admin_password(data[0].username,login_data.new_password)
            response.send("password updated successfully")
          }
          // if fetching data count is empty it sends the response as user not found
          else {
            response.send("Please enter correct username or security question");
          }
        }
      });
     }else{
      //  var message = user_alredy_login(login_data.username)
      //  response.send(JSON.stringify(message))
      response.send("your not a loged in user")
     }
   }
 })
  }
}

/**
 * validate the user login data based on the user register database data
 * @param login_data that contains the user login data  
 * @param response which is the response object that send the response to the client
 * @returns the different response like if username not found or password not correct or login success
 * based on the conditions with validating the user register database data
 */
 function validate_admin_login_data(login_data,response){
    // fetching the admin login data
    ADMIN_LOGIN_DATA.find({"username":`${login_data.username}`},(err,data)=>{
      if(err){
        console.log(err)
      }else{
        if(data.length === 0){
          // fetching the admin register data from the data base for login validatinn
    ADMIN_REGISTER_DATA.find({ username: login_data.username }, (err, data) => {
      // any error occur in the data fetching it prints in the console
      if (err) {
        // printing error in console
        console.log(err);
        //if there is no error in the data fetching it goes through the else block
      } else {
        // if there any data that matches with the username it goes through the if condition
        if (data.length !== 0) {
          // iterating over the data fetched from the database
          for (let item of data) {
            // validating the password
            if (item.password === login_data.password) {
              // if password is valid it sends the response as 200 'success'
              // const x = is_login_before(login_data)
              // console.log(x)
              const jwt_token = jwt_token_generator(login_data.username);
              login_data.jwt_token = jwt_token;
              inserting_admin_login_data_into_database(login_data);
              response.send(login_data);
              break;
            }
            // if password is invalid it sends the response as password not match
            else {
              response.send("password not match");
              break;
            }
          }
        }
        // if fetching data count is empty it sends the response as user not found
        else {
          response.send("user not found");
        }
      }
    });
    }else{
      var message = user_alredy_login(login_data.username)
      response.send(JSON.stringify(message))
      }
    }
  })
}


/**
 * validates the password for various conditions
 * @param {1} password which has to be validated 
 * @returns true - if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.  
 */
function _validate_password(password) {
    if (typeof(password) !== "string") {
        return {status_code:406, status_message:"Password should be a string!"};
    }
    if ((password.length < 3) || (password.length > 15)) {
        return {status_code:411, status_message:"Length of password should be in between 3 and 15"};
    }
    let special_chars=["!","@","#","$","%","^","&","*"];
    var special_len=0;
    for (let item of password){
        if (special_chars.includes(item)) {
            special_len += 1;
        }
    }
    if (special_len ===0) {
        return {status_code:406, status_message:"Password should contain atleast one special character"};
    }
    return {status_code:200};
}

/**
 * validates the re_enter_password for various conditions
 * @param {1} password which has to be validated 
 * @returns 200 status_code if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.  
 */
function re_enter_password_validator(password,re_enter_password){
    if(password !== re_enter_password){
        return {status_code:406,status_message:"re_enter_password not matched with the password"}
    }else{
        return {status_code:200,status_message:"success"}
    }
}

/**
 * validates the email which contains the @gmail.com at end of the email
 * @param {1} email which has to be validated 
 * @returns 200 status_code if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.  
 */
function validate_email(email){
    if(!(email.endsWith("@gmail.com"))){
        return {status_code:406,status_message:"entered email is invalid"}
    }else{
        return {status_code:200,status_message:"success"}
    }
}

/**
 * validates the phone_number which contains the 10 digits in the number or not
 * @param {1} number which has to be validated 
 * @returns 200 status_code if all conditions are satisfied, 
 *          otherwise returns status_code & status_message for which the condition fails.  
 */
function validate_phone_number(number){
    if(number.length !== 10 ){
        return {status_code:406,status_message:"please enter a valid 10 digit phone number"}
    }else{
        return {status_code:200,status_message:"success"}
    }
}

/* 
    exporting the validate_register_data, validate_user_login_data_for_forget_password,  _validate_password, validate_user_register_database,validat_user_register_database
    validate_admin_register_database functions it validate the authentication process based on the user data
*/
export {
    validate_register_data,
    _validate_password,
    validate_user_register_database,
    validate_admin_register_database,
    validate_user_login_data,
    validate_admin_login_data,
    validate_user_login_data_for_forget_password,
    validate_admin_login_data_for_forget_password
    };