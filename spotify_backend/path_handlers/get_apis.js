import {SPOTIFY_ARTIST_DETAILS} from '../utils/mongo_db.js'
/**
 * 
 */
const get_user_data_handler = (request,response)=>{
    response.send(JSON.stringify("hello"))
}

console.log("hello world")

/**
 * 
 */
 const get_artist_details_handler = (request,response)=>{
    SPOTIFY_ARTIST_DETAILS.find({},(err,data)=>{
        if(err){
            console.log(err)
        }else{
            response.send(data)
        }
    })
}

/**
 * 
 */
 const popular_songs_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const related_artist_data_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const latest_release_data_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const album_data_handler = (request,response)=>{
    response.send("hello")
}

/**
 * 
 */
 const playlist_data_handler = (request,response)=>{
    response.send("hello")
}

// exporting all handler_functions 
export{
       get_user_data_handler,
       get_artist_details_handler,
       playlist_data_handler,
       popular_songs_handler,
       album_data_handler,
       latest_release_data_handler,
       related_artist_data_handler
      }