// importing express from the express module
import express from 'express'
import bodyParser from "body-parser";
//Creating an instance of express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));

console.log("hello world")
import cors from 'cors'
app.use(cors({
    origin:"*",
}))
// importing all get_handlers from the get_api's module
import{
       get_user_data_handler,
       get_artist_details_handler,
       playlist_data_handler,
       popular_songs_handler,
       album_data_handler,
       latest_release_data_handler,
       related_artist_data_handler
       } 
       from './path_handlers/get_apis.js'

// importing the login_user_handler and register_user_handler from the postapis file
import{
    login_user_handler,
    register_user_handler
    }
     from './path_handlers/post_apis.js'

// importing the forgot_user_handler from the putapis file
import{
    forgot_user_handler
    }
    from './path_handlers/put_apis.js'

// getting user_data from db and sending it to client
app.get("/user_data",get_user_data_handler)

// getting user_data from db and sending it to client
app.get("/artist_details",get_artist_details_handler)

// getting user_data from db and sending it to client
app.get("/popular_songs",popular_songs_handler)

// getting user_data from db and sending it to client
app.get("/related_artist",related_artist_data_handler)

// getting user_data from db and sending it to client
app.get("/latest_release",latest_release_data_handler)

// getting user_data from db and sending it to client
app.get("/album_data",album_data_handler)

// getting user_data from db and sending it to client
app.get("/playlist/album_id",playlist_data_handler)

// getiing the user data from client and store it into database
app.post("/register/:user_type", register_user_handler)

// validating the user data and given authentication
app.post("/login/:user_type", login_user_handler)

// updating the user password
app.put("/forget/:user_type", forgot_user_handler)




// this server is listening the 3000 port
app.listen(3001,()=>{
    console.log("http://localhost:3001")
})